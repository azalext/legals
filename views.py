# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from azalinc.shortcuts import *

from .tasks import *

################################################################################

def homepage(request):
    context = dict(
        listing=ShopifySite.objects.all(),
    )

    return render(request,'site_list.html', context)

################################################################################

def site_refresh(request, uid):
    shopping_download.delay(uid)

    return HttpResponseRedirect(request.META.get('HTTP_REFERER','/#sync'))

################################################################################

def site_landing(request, uid):
    qs = ShopifySite.objects.filter(Q(pk=uid) or Q(id=uid))

    if len(qs):
        context = dict(
            site=qs[0],
            data=read_json(qs[0].path('products.json')),
        )

        return render(request,'site_view.html', context)
    else:
        raise Http404("Site with PK:='%s' not found !" % uid)

def site_product(request, uid, pid):
    qs = ShopifySite.objects.filter(Q(pk=uid) or Q(id=uid))

    if len(qs):
        context = dict(
            site=qs[0],
            item=None,
        )

        for entry in read_json(qs[0].path('products.json')):
            if pid in (entry['id'], str(entry['id'])):
                context['item'] = entry

        if context['item'] is not None:
            context['product'] = context['item']

            return render(request,'product.html', context)
        else:
            raise Http404("Product with PK:='%s' not found !" % pid)
    else:
        raise Http404("Site with PK:='%s' not found !" % uid)

################################################################################

def site_dataset(request, uid):
    qs = ShopifySite.objects.filter(Q(pk=uid) or Q(id=uid))

    if len(qs):
        ds = read_json(qs[0].path('products.json'))

        rs = {}

        for entry in ds:
            rs[str(entry['id'])] = entry

        return HttpResponse(json.dumps(rs.values()))
    else:
        raise Http404("Site with PK:='%s' not found !" % uid)
