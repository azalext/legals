# -*- coding: utf-8 -*-

from azalinc.shortcuts import *

from azalinc.node.models import *

#*********************************************************************

BACKEND_TYPEs = (
    ('cache', "Memcache daemon"),
    ('redis', "Redis or Sentinel"),

    ('sqldb', "MySQL or PostgreSQL"),
    ('nosql', "MongoDB database"),
    ('neo4j', "Neo4j instance"),

    ('parse', "Parse Server"),
    ('graph', "GraphQL engine"),

    ('queue', "AMQP instance"),
    ('topic', "MQTT instance"),
)

################################################################################

class JuridicField(models.Model):
    alias    = models.CharField(max_length=64)
    title    = models.CharField(max_length=128)

    def __str__(self): return str(self.alias)

    class Meta:
        verbose_name = "Juridic Field"
        verbose_name_plural = "Juridic Fields"

#*******************************************************************************

class Profession(models.Model):
    alias    = models.CharField(max_length=64)
    title    = models.CharField(max_length=128)

    def __str__(self): return str(self.alias)

    class Meta:
        verbose_name = "Profession"
        verbose_name_plural = "Professions"

#*******************************************************************************

class Committee(models.Model):
    alias    = models.CharField(max_length=64)
    title    = models.CharField(max_length=128)

    def __str__(self): return str(self.alias)

    class Meta:
        verbose_name = "Committee"
        verbose_name_plural = "Committees"

################################################################################


