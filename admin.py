# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import *

####################################################################

class FieldAdmin(admin.ModelAdmin):
    list_display  = ['alias','title']
    #list_filter   = ['user__username']
    #list_editable = ['title']

admin.site.register(JuridicField, FieldAdmin)

#*******************************************************************

class ProfessionAdmin(admin.ModelAdmin):
    list_display  = ['alias','title']
    #list_filter   = ['user__username']
    #list_editable = ['title']

admin.site.register(Profession, ProfessionAdmin)

#*******************************************************************

class CommitteeAdmin(admin.ModelAdmin):
    list_display  = ['alias','title']
    #list_filter   = ['user__username']
    #list_editable = ['title']

admin.site.register(Committee, CommitteeAdmin)

