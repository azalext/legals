from controlcenter import Dashboard, widgets

from azalext.legals.models import *

################################################################################

class JuridicList(widgets.ItemList):
    model = JuridicField
    list_display = ('pk', 'username','email')

#*******************************************************************************

class ProfessionList(widgets.ItemList):
    model = Profession
    list_display = ('pk', 'username','email')

#*******************************************************************************

class CommitteeList(widgets.ItemList):
    model = Committee
    list_display = ('pk', 'username','email')

################################################################################

class MySingleBarChart(widgets.SingleBarChart):
    # label and series
    values_list = ('username', 'score')
    # Data source
    queryset = Person.objects.order_by('-score')
    limit_to = 3

################################################################################

class Landing(Dashboard):
    title = 'Legals'

    widgets = (
        JuridicList,
        ProfessionList,
        CommitteeList,

    )

